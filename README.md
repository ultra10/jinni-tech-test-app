
# Install

`composer install`

# Config

Located in config.ini, has resultsAmount - how much lottery result will be save, now set to 3

# Run

`php run.php`

# Result

located in `result` folder

# Task
Z podanych 3 loterii chcielibyśmy abyś ściągnął dane i zapisał wyniki losowań do pliku w formacie json. Nie można używać "file_get_contents" i "fopen".


https://www.elgordo.com/results/euromillonariaen.asp 

http://www.lotto.pl/lotto/wyniki-i-wygrane

http://www.lotto.pl/eurojackpot/wyniki-i-wygrane 

Skrypt powinien działać z poziomu php cli. Stosujemy PSR, PHP 7.2 pełne OOP – używanie jednej klasy jako opakowania dla strukturalnego kodu będzie dyskwalifikowane.

Do testu można też dodać alternatywną technologię „NodeJS, Framework: NestJS” – do wyboru.  