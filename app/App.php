<?php

require("utils/WebGetter.php");
require("utils/FileHelper.php");
require("importers/Importer.php");

class App {

    public static $instance;

    public $resultsAmount;

    public function __construct(array $config) {
        echo "[APP] Start app\n";

        $this->resultsAmount = $config["resultsAmount"];

        self::$instance = $this;

        $this->webGetter = new WebGetter(); 
        $this->fileHelper = new FileHelper(); 

        $importers = [
            "Lotto",
            "ElgordoResults",
            "LottoEurojackpot"
        ];
       
        
        for($i = 0; $i < count($importers); $i ++) {
            require("importers/".$importers[$i].".php");
            new $importers[$i]();
        }
       
    }

    public static function getInstance() : App {
        return self::$instance;
    }
}