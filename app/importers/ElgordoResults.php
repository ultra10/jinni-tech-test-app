<?php
declare(strict_types=1);

use voku\helper\HtmlDomParser;

class ElgordoResults extends Importer{
    public function __construct() {
        parent::__construct();

        $this->mainUrl = "https://www.elgordo.com/results/euromillonariaen.asp";
        $this->run();   
    }

    private function run() {
        $data = [];

        $this->nextSortParam = null;
        
        while(count($data) < $this->app->resultsAmount) {
            $params = "";
            if($this->nextSortParam != null) {
                $params = "?sort=" . $this->nextSortParam;
            }
            $d = $this->scrap($this->mainUrl . $params);
            if(count($d->numbers) > 0) {
                $data[] = $d;
            }
        }

        $this->app->fileHelper->saveToFile("result/elgordo.json", json_encode($data));
    }

    private function scrap(string $url): stdClass {
        echo "[ELGORDO] get site content: " . $url . "\n";

        $html = $this->getHtml($url);

        if(strlen($html) > 0) {
            $dom = HtmlDomParser::str_get_html($html);

            $param = $dom->findOne('#center .center .prev a');
            $param->getAllAttributes();
            $param = explode("=", $param->getAllAttributes()["href"]);
            $this->nextSortParam = $param[1];

            $numbers = [];

            // find numbers
            $elements = $dom->findMulti('.int-num');
            foreach ($elements as $el) {
                $numbers[] = $el->innertext;
            }
            
            // find date
            $dateNode = $dom->findOne('#center .center .body_game .c');
            $date = str_replace("&nbsp;", " ", $dateNode->innertext);
            echo "[ELGORDO] Found | " . $date . " | numbers: " . implode ( "," , $numbers ) . "\n";
            return (object) array(
                "date" => $date,
                "numbers" => $numbers
            );
        } else {
            return (object) array(
                "date" => "",
                "numbers" => []
            );;
        }
    }
}