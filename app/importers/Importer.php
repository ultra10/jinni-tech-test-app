<?php
declare(strict_types=1);

abstract class Importer
{
    public function __construct() {
        $this->app = App::getInstance();
    }

    public function getHtml(string $url, array $post = null): string {
        $data = $this->app->webGetter->getContent($url, $post);
        
        if($data->code == 200) {
            return $data->data;
        } else if($data->code == 429) { // too many request, neet to w8 some
            echo "[IMPORTER] Too many request wait " . $data->retryAfter . " seconds\n";
            sleep($data->retryAfter);
            return $this->getHtml($url, $post);
        } else {
            return "";
        }
    }

    public function getNextDay(string $d): string {
        $date = new DateTime($d);
        $date->modify('-1 day');
        return $date->format('o-m-d');
    }
}