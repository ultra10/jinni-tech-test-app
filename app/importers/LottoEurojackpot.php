<?php
declare(strict_types=1);

use voku\helper\HtmlDomParser;

class LottoEurojackpot extends Importer {
    public function __construct() {
        parent::__construct();
 
        $this->mainUrl = "https://www.lotto.pl/eurojackpot/wyniki-i-wygrane/wyszukaj";
        $this->run();   
    }

    private function run() {
        $data = [];

        $date = date("o-m-d");
        while(count($data) < $this->app->resultsAmount) {
            $d = $this->scrap($this->mainUrl, $date);
            if(count($d->numbers) > 0) {
                $data[] = $d;
            } else {
                echo "[EUROJACKPOT] Lottery data not found\n";
            }  
            
            $date = $this->getNextDay($date);
        } 
          
        $this->app->fileHelper->saveToFile("result/eurojackpot.json", json_encode($data)); 
    }


    private function scrap(string $url,string $date): stdClass {
        echo "[EUROJACKPOT] get site content: " . $url . " | date: " . $date . "\n";
        $post = [
            'data_losowania[date]' => $date,
            'form_id' => 'eurojackpot_wyszukaj_form'
        ];

        $html = $this->getHtml($url, $post);

        if($html !== null) {
            $dom = HtmlDomParser::str_get_html($html);
            
            $numbers = [];
            
            // find numbers
            $ow = $dom->findMulti('.ostatnie-wyniki-table');
            if(count($ow) > 0) {
                $elements = $ow[1]->findMulti('.sortkolejnosc .number span');
                foreach ($elements as $el) {
                    if($el->innertext != "+") {
                        $numbers[] = $el->innertext;
                    }
                }
                
                // find date
                $dateNode = $ow[1]->findOne('.contentData span');
                $data = explode("<br>", $dateNode->innertext); 
                $gameNumber = explode(" ", $data[0]);
                $gameNumber = $gameNumber[count($gameNumber) - 1];
                $date = explode(",", $data[1]);
                $date = $date[0];
        
                echo "[EUROJACKPOT] Found | " . $gameNumber . " | " . $date . " | numbers: " . implode ( "," , $numbers ) . "\n";
                return (object) array(
                    "lotteryNr" => $gameNumber,
                    "date" => $date,
                    "numbers" => $numbers
                );
            } else {
                return (object) array(
                    "lotteryNr" => null,
                    "date" => "",
                    "numbers" => []
                );
            }
        } else {
            return (object) array(
                "lotteryNr" => null,
                "date" => "",
                "numbers" => []
            );
        }
    }
}