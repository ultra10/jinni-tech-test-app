<?php
declare(strict_types=1);

class FileHelper {
    public function __construct() {
    }

    public function saveToFile(string $fileName, string $data) {
        echo "[FILE HELPER] Save data to file: " . $fileName . "\n";
        file_put_contents($fileName, $data);
    }
}