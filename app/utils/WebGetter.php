<?php
declare(strict_types=1);

class WebGetter {
    public function __construct() {
        echo "[WEB GETTER] Init\n";
        $this->defaultRetryAfter = 5; // in seconds
    }

    public function getContent(string $url,array $postData = null): stdClass {

        $ch = curl_init();
        $timeout = 5;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, true);
        
        if($postData !== null) {
            curl_setopt($ch, CURLOPT_POST, true);
            $postData = http_build_query($postData);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            
            $headers = array( 
                'Content-Length: ' . strlen($postData),
                'Content-Type: application/x-www-form-urlencoded'
            );
   
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        }
   
        $response = curl_exec($ch);
        $inf = curl_getinfo($ch);
  
        $body = substr($response, $inf["header_size"]);

        if($inf["http_code"] == 429) {
            $header = substr($response, 0, $inf["header_size"]);
            $header = explode("\n", $header);
            $retryAfter = $this->defaultRetryAfter;
            for($i = 0; $i < count($header); $i++) {
                if (strpos($header[$i], 'Retry-After') !== false) {
                    $ra = explode(":", $header[$i]);
                    $retryAfter = intval($ra[1]);
                }
            }
            curl_close($ch);
            return (object) array(
                "code" => $inf["http_code"],
                "data" => $body,
                "retryAfter" => $retryAfter
            );
        } else{
            curl_close($ch);
            return (object) array(
                "code" => $inf["http_code"],
                "data" => $body
            );
        }
    }
}