<?php


error_reporting(E_ALL);


require_once ('vendor/autoload.php');

require("app/App.php");

$config = parse_ini_file('config.ini');

new App($config);

